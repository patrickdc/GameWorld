<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Game World</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
    <script src="main.js"></script>
</head>

<body>
    <div id="AllContainerPvlt">
        <?php 
            include ("nav.php");
        ?>

        <div id="MainDivPvlt">
            <div id="MainTopContainerPvlt">
                <h1>Cart</h1>
            </div>

            <div id="MainDefContainerPvlt">
                <table id="CartTablePvlt">
                    <thead>
                        <tr>
                            <th class="TableImgPvlt">Image</th><th>Game</th><th>Description</th><th>Total</th><th>Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="TableImgPvlt"><img src="images/games/pc/rocketleague.jpg"></td>
                            <td class="TableGamePvlt">Rocket League</td>
                            <td class="TableDescPvlt">Rocket League is a vehicular soccer video game developed and published by Psyonix. The game was first released for Microsoft Windows and PlayStation 4 in July 2015, with ports for Xbox One, macOS, Linux, and Nintendo Switch being released later on. In June 2016, 505 Games began distributing a physical retail version for PlayStation 4 and Xbox One, with Warner Bros. Interactive Entertainment taking over those duties by the end of 2017. </td>
                            <td class="TablePricePvlt">€15,00</td>
                            <td class="TableQuantPvlt">1</td>
                        </tr>
                        <tr>
                            <td class="TableImgPvlt"><img src="images/games/ps/jumpforce.jpg"></td>
                            <td class="TableGamePvlt">Jump Force</td>
                            <td class="TableDescPvlt">Jump Force is a fighting game developed by Spike Chunsoft and published by Bandai Namco Entertainment featuring characters from various manga series featured in the Weekly Shōnen Jump anthology in celebration of the magazine's 50th anniversary.</td>
                            <td class="TablePricePvlt">€15,00</td>
                            <td class="TableQuantPvlt">1</td>
                        </tr>
                        <tr>
                            <td class="TableImgPvlt"><img src="images/games/n64/oot.jpg"></td>
                            <td class="TableGamePvlt">Ocarina of Time</td>
                            <td class="TableDescPvlt">The Legend of Zelda: Ocarina of Time is an action-adventure game developed and published by Nintendo for the Nintendo 64. It was released in Japan and North America in November 1998, and in Europe and Australia the following month.</td>
                            <td class="TablePricePvlt">€15,00</td>
                            <td class="TableQuantPvlt">1</td>
                        </tr>
                        <tr>
                            <td class="TableImgPvlt"></td>
                            <td class="TableGamePvlt"></td>
                            <td class="TableDescPvlt"></td>
                            <td class="TablePricePvlt">Total:€45,00</td>
                            <td class="TableQuantPvlt"><img src="images/order.png" id="orderImgPvlt"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
                <div id="BottomNavContainerPvlt">
                    <a href="games.php?GameCategory=2"><div class="NavDivPvlt" id="NavPlaystationPvlt"><h1>Playstation</h1></a></div>
                    <a href="games.php?GameCategory=1"><div class="NavDivPvlt" id="NavN64Pvlt"><h1>N64</h1></div></a>
                    <a href="games.php?GameCategory=3"><div class="NavDivPvlt lastNav" id="NavPCPvlt"><h1>PC</h1></div></a>
                    <div id="clear"></div>
                </div>
        </div>

        <?php 
        include ("footer.php");
        ?>
    </div>
</body>

</html>