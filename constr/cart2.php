<?php
//sql database connect 
$cartActive = 0;
$gameQuantity = 0;
include ("db.php");
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Game World</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
    <script src="main.js"></script>
</head>

<body>
    <div id="AllContainerPvlt">
        <?php 
            include ("nav.php");
        ?>

        <div id="MainDivPvlt">
            <div id="MainTopContainerPvlt">
                <h1>Cart</h1>
            </div>

            <div id="MainDefContainerPvlt">
                <table id="CartTablePvlt">
                    <thead>
                        <tr>
                            <th class="TableImgPvlt"></th><th>Game</th><th>Description</th><th>Total</th><th>Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach ($gamesCartArray as $game)
                        { ?>     
                        <tr>
                            <td class="TableImgPvlt"><img src="<?php echo $game['Image'];?>"></td>
                            <td class="TableGamePvlt"><?php echo $game['GameTitle'];?></td>
                            <td class="TableDescPvlt"><?php echo $game['Description'];?></td>
                            <td class="TablePricePvlt"><?php echo "€".$game['GamePrice'];?></td>
                            <td class="TableQuantPvlt"><?php echo $gameQuantity;?></td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <td class="TableImgPvlt"></td>
                            <td class="TableGamePvlt"></td>
                            <td class="TableDescPvlt"></td>
                            <td class="TablePricePvlt">Total:€45,00</td>
                            <td class="TableQuantPvlt"><img src="images/order.png" id="orderImgPvlt"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
                <div id="BottomNavContainerPvlt">
                    <a href="games.php?GameCategory=2"><div class="NavDivPvlt" id="NavPlaystationPvlt"><h1>Playstation</h1></a></div>
                    <a href="games.php?GameCategory=1"><div class="NavDivPvlt" id="NavN64Pvlt"><h1>N64</h1></div></a>
                    <a href="games.php?GameCategory=3"><div class="NavDivPvlt lastNav" id="NavPCPvlt"><h1>PC</h1></div></a>
                    <div id="clear"></div>
                </div>
        </div>

        <?php 
        include ("footer.php");
        ?>
    </div>
</body>

</html>