<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $name = $_POST['name'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $call = $_POST['call'];
    $website = $_POST['website'];
    $priority = $_POST['priority'];
    $type = $_POST['type'];
    $message = $_POST['message'];
    $formcontent=" From: $name \n Phone: $phone \n Call Back: $call \n Website: $website \n Priority: $priority \n Type: $type \n Email: $email \nMessage: $message";
    $recipient = "43874@rocteraa-student.nl";
    $subject = "Contact Form";
    $mailheader = "Sent by: contactform@patrick.nl \r\n";
    mail($recipient, $subject, $formcontent, $mailheader) or die("Error!");
}

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Game World</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
    <script src="main.js"></script>
</head>

<body>
    <div id="AllContainerPvlt">
        <?php 
            include ("nav.php");
        ?>

        <div id="MainDivPvlt">
            <div id="MainTopContainerPvlt">
                <h1>Contact</h1>
            </div>

            <div id="MainDefContainerPvlt">
                <form action="contact.php" method="POST">
                    Name <input type="text" name="name" required><br>
                    Email <input type="text" name="email" required><br>
                    Phone <input type="text" name="phone" required><br>
                    <br>
                    Request Phone Call:<br>
                    Yes:<input type="checkbox" value="Yes" name="call"><br>
                    No:<input type="checkbox" value="No" name="call"><br>
                    <br>
                    Website <input type="text" name="website"><br>
                    <br>
                    Priority
                    <select name="priority" size="1">
                        <option value="Low">Low</option>
                        <option value="Normal">Normal</option>
                        <option value="High">High</option>
                        <option value="Emergency">Emergency</option>
                    </select>
                    <br>
                    Type
                    <select name="type" size="1">
                        <option value="update">Website Update</option>
                        <option value="change">Information Change</option>
                        <option value="addition">Information Addition</option>
                        <option value="new">New Products</option>
                    </select>

                    <p>Message</p><textarea name="message" rows="6" cols="25"></textarea><br />
                    <input type="submit" value="Send"><input type="reset" value="Clear">
                </form>    
            </div>

            <div id="BottomNavContainerPvlt">
                <a href="games.php?GameCategory=2"><div class="NavDivPvlt" id="NavPlaystationPvlt"><h1>Playstation</h1></a></div>
                <a href="games.php?GameCategory=1"><div class="NavDivPvlt" id="NavN64Pvlt"><h1>N64</h1></div></a>
                <a href="games.php?GameCategory=3"><div class="NavDivPvlt lastNav" id="NavPCPvlt"><h1>PC</h1></div></a>
                <div id="clear"></div>
            </div>
        </div>

        <?php 
        include ("footer.php");
        ?>
    </div>
</body>

</html>