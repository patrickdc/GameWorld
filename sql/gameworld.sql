-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 14, 2018 at 09:36 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gameworld`
--
CREATE DATABASE IF NOT EXISTS `gameworld` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `gameworld`;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `CategoryID` int(3) NOT NULL,
  `CategoryTitle` varchar(100) NOT NULL,
  `CategoryDescription` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`CategoryID`, `CategoryTitle`, `CategoryDescription`) VALUES
(1, 'N64', 'The Nintendo 64 (codenamed \"Project Reality\"; sometimes referred as the \"N64\") is a video game console created by Nintendo. It was released in 1996 to compete with the Sega Saturn and the Sony PlayStation.\r\n\r\nDespite not beating the PlayStation\'s sales, the Nintendo 64 was still largely successful for many reasons, one of the big ones being the release of Super Mario 64. Super Mario 64 was one of the first games of its kind to feature full 3D graphics and depth of field effects. The Nintendo 64 was able to pull this off because it was the first system to feature a 64-bit processor and 32-bit graphics chip (aside from the failed Atari Jaguar, which was really multiple co-processors using 64 bit architecture on a 32-bit main processor). The Nintendo 64 also featured the first successful analog control stick implementation and four built-in controller ports, unlike its competitors, the PlayStation and the Sega Saturn. The Nintendo 64 is also noted as the last home console system to use cartridges until the Nintendo Switch.'),
(2, 'Playstation', 'In the seven years since the introduction of the PlayStation 3, we\'ve seen our gaming consoles transform into living-room hubs through constant evolution and software updates. Those updates weren\'t always smooth – though on PS3, they were always happening – but it\'s easy to see just how far the platform has come.'),
(3, 'PC', '\"PC\" is an initialism for \"personal computer\". The IBM Personal Computer incorporated the designation in its model name, but IBM has not used this brand for many years. It is sometimes useful, especially in a marketing context, to distinguish personal computers of the \"IBM Personal Computer\" family from personal computers made by other manufacturers. For example, \"PC\" is used in contrast with \"Mac\", an Apple Macintosh computer.[5][6][7][8] This sense of the word is used in the \"Get a Mac\" advertisement campaign that ran between 2006 and 2009, as well as its rival, I\'m a PC campaign, that appeared in 2008. Since none of these Apple products were mainframes or time-sharing systems, they were all \"personal computers\" and not \"PC\" (brand) computers.');

-- --------------------------------------------------------

--
-- Table structure for table `games`
--

CREATE TABLE `games` (
  `GameID` int(3) NOT NULL,
  `GameTitle` varchar(100) NOT NULL,
  `GamePrice` decimal(5,2) NOT NULL,
  `GameCategory` int(3) NOT NULL,
  `Description` varchar(65000) DEFAULT NULL,
  `Image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `games`
--

INSERT INTO `games` (`GameID`, `GameTitle`, `GamePrice`, `GameCategory`, `Description`, `Image`) VALUES
(1, 'Super Mario', '25.12', 1, 'By now, many Mario fans have taken a look at either the Japanese or U.S. version of Super Mario 64, so we\'ll spare you (most of) the hype and rhetoric. Having said that, it must be stated that SM64 is the greatest videogame to date, and one which all games, regardless of genre, will be judged henceforth. Nintendo set itself a nearly impossible goal and achieved it with SM64. Look closely, Mario\'s lineage is 2D -- not ideal material upon which to base the most intensely scrutinized 3D videogame ever. But videogames are, in the end, meant to entertain. And entertainment is at the heart of this fantastic title.', 'images/games/n64/supermario.png'),
(2, 'Legend of Zelda: Ocarina of Time', '12.13', 1, 'The new benchmark for interactive entertainment has arrived. The Legend of Zelda: Ocarina of Time, the fifth official installment in Nintendo\'s popular action adventure series is finally here, and like its NES predecessor in 1987 it is a game so enjoyable, it has the power to pull videogame players into its imaginative worlds -- and refuse to let go for days. Call us crazy, but when the final version of Zelda 64 arrived in the IGN64 offices, we stopped working, locked ourselves into a room with a big-screen TV and a surround system and played 17 hours straight. After only a few hours of sleep, we were back for more and we couldn\'t stop until we finished the game. Then, we started over again to find all the secrets.', 'images/games/n64/oot.jpg'),
(3, 'Super Smash Bros', '14.00', 1, 'Super Smash Bros. Ultimate is a 2018 crossover fighting game developed by Bandai Namco Studios and Sora Ltd. and published by Nintendo for the Nintendo Switch. It is the fifth installment in the Super Smash Bros. series, succeeding Super Smash Bros. for Nintendo 3DS and Wii U.', 'images/games/n64/smash.jpg'),
(4, 'Donkey Kong', '16.00', 1, 'Donkey Kong 64 is a 1999 adventure platform video game for the Nintendo 64 console, and the first in the Donkey Kong series to feature 3D gameplay. As the gorilla Donkey Kong, the player explores the themed levels of an island to collect items and rescue his kidnapped friends from K. Rool.', 'images/games/n64/donkey.jpg'),
(5, 'Banjo Kazooie', '18.00', 1, 'Banjo-Kazooie is a platform video game developed by Rare and originally released for the Nintendo 64 video game console in 1998.', 'images/games/n64/banjo.jpg'),
(6, 'Mario Kart', '19.00', 1, 'Mario Kart 64 is a kart racing video game developed and published by Nintendo for the Nintendo 64 video game console. It is the successor to Super Mario Kart for the Super Nintendo Entertainment System, and the second game in the Mario Kart series. ', 'images/games/n64/mariokart.jpg'),
(10, 'Spider-Man', '20.00', 2, 'Marvel\'s Spider-Man is an action-adventure game developed by Insomniac Games and published by Sony Interactive Entertainment for the PlayStation 4, based on the Marvel Comics superhero Spider-Man. Released worldwide on September 7, 2018, it was the first licensed game developed by Insomniac. ', 'images/games/ps/spiderman.jpg'),
(11, 'Kingdom Hearts', '25.00', 2, 'Kingdom Hearts III is an upcoming action role-playing video game developed and published by Square Enix for the PlayStation 4 and Xbox One. It is the twelfth installment in the Kingdom Hearts series, a sequel to Kingdom Hearts II, and the final chapter in the Dark Seeker saga.', 'images/games/ps/kingdomhearts.jpg'),
(12, 'Black-Ops 4', '20.00', 2, 'Call of Duty: Black Ops 4 is a multiplayer first-person shooter developed by Treyarch and published by Activision. It was released worldwide for Microsoft Windows, PlayStation 4, and Xbox One on October 12, 2018.', 'images/games/ps/blackops.jpg'),
(13, 'Jump Force', '30.00', 2, 'For the first time ever, the most famous Manga heroes are thrown into a whole new battleground: our world. Uniting to fight the most dangerous threat, the Jump Force will bear the fate of the entire human kind.\r\n\r\nCelebrating the 50th Anniversary of the famous Weekly Jump Magazine, Jump Force is also making the most of latest technologies to bring characters to life in a never-seen-before realistic design.', 'images/games/ps/jumpforce.jpg'),
(14, 'Spyro', '30.00', 2, 'Spyro Reignited Trilogy is a platform video game developed by Toys for Bob and published by Activision. It is a collection of remasters of the first three games in the Spyro series: Spyro the Dragon, Spyro 2: Ripto\'s Rage! and Spyro: Year of the Dragon. ', 'images/games/ps/spyro.jpg'),
(16, 'Rocket League', '20.00', 3, 'Rocket League is a vehicular soccer video game developed and published by Psyonix. The game was first released for Microsoft Windows and PlayStation 4 in July 2015, with ports for Xbox One, macOS, Linux, and Nintendo Switch being released later on.', 'images/games/pc/rocketleague.jpg'),
(17, 'Elder Scrolls Online', '19.50', 3, 'The Elder Scrolls Online is a massively multiplayer online role-playing video game developed by ZeniMax Online Studios and published by Bethesda Softworks. It was released for Microsoft Windows and OS X in April 2014. It is a part of The Elder Scrolls series, of which it is the first multiplayer installment.', 'images/games/pc/eso.jpg'),
(18, 'Ironsight', '10.00', 3, 'Ironsight is a free-to-play shooter set in the 2020s, when two factions fight over the last of the planet’s dwindling resources. Built from the ground up in the Iron Engine, Ironsight offers top-notch visuals and dynamic battlefields. You’ll need both a quick trigger finger and tactical skills to overcome your enemies on 20 combat zones with destructible elements. Fight against the terrain, your enemies, and the elements themselves on your path to victory!', 'images/games/pc/ironsight.jpg'),
(19, 'Rainbow Six Siege', '15.00', 3, 'Tom Clancy\'s Rainbow Six Siege is a tactical shooter video game developed by Ubisoft Montreal and published by Ubisoft. It was released worldwide for Microsoft Windows, PlayStation 4, and Xbox One on December 1, 2015. The game puts heavy emphasis on environmental destruction and cooperation between players.', 'images/games/pc/rainbow.jpg'),
(20, 'Counter Strike: GO', '10.00', 3, 'Counter-Strike: Global Offensive is arguably one of the most iconic FPS titles of its generation. The new Panorama UI drastically improves the look and feel, but the gameplay can be repetitive and some maps look outdated.\r\n\r\nValve\'s Counter-Strike: Global Offensive (CS: GO) debuted in 2012, backed by a strong heritage of multiplayer FPS titles, including the original Counter-Strike and Counter-Strike: Source. Now, six years later, the fast-paced PC game still mostly holds its own against more modern titles, partly because of its established core gameplay and active community. Visually, however, CS: GO is starting to show its age and it\'s not as thematically rich as other popular titles, such as Overwatch. Still, many players will enjoy CS: GO\'s no-frills experience and competitive scene.', 'images/games/pc/csgo.jpg'),
(21, 'Star Wars: Jedi Academy', '25.00', 3, 'About a year-and-a-half ago Raven Software surprised the heck out of us with a little title called Jedi Outcast. It wasn\'t that Raven had made such a great game; we were used to that already from Elite Force and Soldier of Fortune. What really shocked us was how well they captured the license and designed a really solid game around it. And now that Raven has offered up a sequel, Star Wars Jedi Knight: Jedi Academy, they\'ve managed to strengthen many of the features of the previous game. ', 'images/games/pc/jka.jpg'),
(22, 'Red Dead Redemption', '15.00', 2, 'Red Dead Redemption 2 is a Western-themed action-adventure game developed and published by Rockstar Games. It was released on October 26, 2018, for the PlayStation 4 and Xbox One consoles. The third entry in the Red Dead series, it is a prequel to the 2010 game Red Dead Redemption.', 'images/games/ps/reddead.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`CategoryID`);

--
-- Indexes for table `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`GameID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `CategoryID` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `games`
--
ALTER TABLE `games`
  MODIFY `GameID` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
