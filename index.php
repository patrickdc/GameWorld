<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Game World</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
    <script src="main.js"></script>
</head>

<body>
    <div id="AllContainerPvlt">
        <?php 
            include ("nav.php");
        ?>

        <div id="MainDivPvlt">

            <div id="MainTopContainerPvlt">
                <h1>Home</h1>
            </div>
                <div class="MainContainerPvlt">
                    <h1>Welcome to Game World</h1>
                    <p>The most complete webshop!</p>
                </div>

            <div id="BottomNavContainerPvlt">
                <a href="games.php?GameCategory=2"><div class="NavDivPvlt" id="NavPlaystationPvlt"><h1>Playstation</h1></a></div>
                <a href="games.php?GameCategory=1"><div class="NavDivPvlt" id="NavN64Pvlt"><h1>N64</h1></div></a>
                <a href="games.php?GameCategory=3"><div class="NavDivPvlt lastNav" id="NavPCPvlt"><h1>PC</h1></div></a>
                <div id="clear"></div>
            </div>
        </div>

        <?php 
        include ("footer.php");
        ?>
    </div>
</body>

</html>