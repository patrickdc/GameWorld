<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Game World</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
    <script src="main.js"></script>
</head>

<body>
    <div id="AllContainerPvlt">
        <?php 
            include ("nav.php");
        ?>

        <div id="MainDivPvlt">
            <div id="MainTopContainerPvlt">
                <h1>About Us</h1>
            </div>
                <div class="MainContainerPvlt">
                <p>My name is Patrick Veltrop. I'm a student at ROC Ter Aa in Helmond.</p>
                <br><p>Game World is the second web assignment I've made at school.</p>
                The header and footer both reappear with PHP include in every webpage. 
                <br>The category buttons send a Get parameter, which allows the website to retrieve the according games from the SQL database.
                The games are dynamically shown on the website with a foreach loop which loops through the array created by the SQL connection. 
                <br>The cart is still static for the time being, this will be updated. The contact form is functional in PHP, but it won't send an actual mail.
                <br>
                <br>All copyrights belong to their respective owners.</p>
                </div>

            <div id="BottomNavContainerPvlt">
                <a href="games.php?GameCategory=2"><div class="NavDivPvlt" id="NavPlaystationPvlt"><h1>Playstation</h1></a></div>
                <a href="games.php?GameCategory=1"><div class="NavDivPvlt" id="NavN64Pvlt"><h1>N64</h1></div></a>
                <a href="games.php?GameCategory=3"><div class="NavDivPvlt lastNav" id="NavPCPvlt"><h1>PC</h1></div></a>
                <div id="clear"></div>
            </div>
        </div>
        
        <?php 
        include ("footer.php");
        ?>
    </div>
</body>

</html>