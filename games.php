<?php
//sql database connect 
include ("db.php");
?>

<?php
if(($_GET['GameCategory'])==1)
{
    $GameCategoryBG="GamesTopContainerN64Pvlt";
    $GameCatTitle="N64 | ";
    $GameHeader="GameHeaderOne";
}

if(($_GET['GameCategory'])==2)
{
    $GameCategoryBG="GamesTopContainerPlaystationPvlt";
    $GameCatTitle="PS4 | ";
    $GameHeader="GameHeaderTwo";
}

if(($_GET['GameCategory'])==3)
{
    $GameCategoryBG="GamesTopContainerPCPvlt";
    $GameCatTitle="PC | ";
    $GameHeader="GameHeaderThree";
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Game World</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
    <script src="main.js"></script>
</head>

<body>
    <div id="AllContainerPvlt">
        <?php 
            include ("nav.php");
        ?>

        <div id="MainDivPvlt">
    
                <div id="GamesContainerPvlt">
                        <div id="ProductsContainerPvlt">
                            <?php
                                foreach ($gamesArray as $game)
                                { ?> 
                                    <div class="ProductItemPvlt">
                                            <div class="GameHeaderDivPvlt"><h3 class="GameHeader" id="<?php echo $GameHeader;?>"><?php echo $GameCatTitle;?><?php echo $game['GameTitle'];?></h3></div>
                                            <div id="ProductBGPvlt" style="background-image:url('<?php echo $game['Image'];?>')">
                                            </div>
                                                <div id="PriceDiv"><h3 id="PriceTextPvlt">€<?php echo $game['GamePrice'];?></h3>
                                                <a href="cart.php"><img src="images/cart.png" id="ImgOrderPvlt"></a>
                                                </div>
                                    </div> 
                            <?php } ?>
                        </div>
                </div>
            <div id="clear"></div>

            <div id="BottomNavContainerPvlt">
                <a href="games.php?GameCategory=2"><div class="NavDivPvlt" id="NavPlaystationPvlt"><h1>Playstation</h1></a></div>
                <a href="games.php?GameCategory=1"><div class="NavDivPvlt" id="NavN64Pvlt"><h1>N64</h1></div></a>
                <a href="games.php?GameCategory=3"><div class="NavDivPvlt lastNav" id="NavPCPvlt"><h1>PC</h1></div></a>
                <div id="clear"></div>
            </div>
        </div>

        <?php 
        include ("footer.php");
        ?>
    </div>
</body>

</html>